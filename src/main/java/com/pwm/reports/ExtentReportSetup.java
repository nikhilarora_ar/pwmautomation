package com.pwm.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.pwm.BaseClass.TestBase;
import com.pwm.utilities.SlackUtility;
import com.pwm.utilities.TestUtility;

public class ExtentReportSetup extends TestBase
{	
	public static ExtentReports extent;
	public static ExtentTest extentTest;
	public static ExtentSparkReporter sparkReport;
	public static String testReportPath;
	
	
	public static ExtentReports extentReportSetup() throws Exception
	{
		 SlackUtility slackUtility=new SlackUtility();
		testReportPath= System.getProperty("user.dir") + "/PWMExtentResults/PWMExtentReport" + TestUtility.getSystemDate() + ".html";
		sparkReport = new ExtentSparkReporter(testReportPath);
		
		extent = new ExtentReports();
		extent.attachReporter(sparkReport);
		
		sparkReport.config().setTheme(Theme.DARK);
		sparkReport.config().setReportName("Test Automation Report");
		sparkReport.config().setDocumentTitle("Test Automation Report");
		slackUtility.sendTestExecutionStatusToSlack("test case started-----");
		
		return extent;
	}
}
