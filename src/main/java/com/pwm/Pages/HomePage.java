package com.pwm.Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.pwm.BaseClass.TestBase;
import com.pwm.utilities.TestUtility;

import Constants.Constants;

public class HomePage extends TestBase {
	
	@FindBy(xpath="//div[@class='sc-pNWdM XJxhY']/div")
	WebElement loggedInUser;
	
	@FindBy(id="mui-component-select-taxStatus")
	WebElement selectRM;
		
	@FindBy(xpath="//*[@id=\"menu-taxStatus\"]//div/ul/li[text()='Self']")
	WebElement selfRM;
	
	@FindBy(xpath="//*[@id=\"menu-taxStatus\"]//div/ul/li[text()='937524 (Tejas Gaykar)']")
	WebElement adminLogin;
		
	@FindBy(xpath="//button[text()='Confirm']")
	WebElement confirmRM;
	
	@FindBy(xpath="//div[@class='dropdown-heading-value']")
	WebElement filterByStatus;
	
		
	@FindBy(xpath="//input[@type='checkbox']/ancestor::div[@class='select-panel']/ul/li//span[text()='Approval pending by Client']")
	WebElement approvalPendingByClientStatus;
	
	
	@FindBy(xpath="//*[contains(text(),'Total UCC')]/parent::span")
	WebElement totalUCC;
	
	@FindBy(xpath="//div[@class='sc-iemWCZ hKymwP']/div")
	List<WebElement> columnsList;
	
			
	@FindBy(xpath="//div[contains(@class,'ReactVirtualized__Grid__innerScrollContainer')]/div")
	List<WebElement> clientList;
	
		
	@FindBy(xpath="//button[contains(@class,'create-client__btn')]")
	WebElement createClientButton;
	
		
	@FindBy(id="mui-component-select-taxStatus")  
	WebElement taxStatusDropDown;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']")  //multiple li under this
	WebElement taxStatusListBox;
	
	@FindBy(id="mui-component-select-holdingNature")
	WebElement holdingStatusDropDown;
	
	/*
	 * @FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']")
	 * //multiple li under this WebElement holdingStatusListBox;
	 */
	
	@FindBy(xpath="//button[text()='Proceed']")
	WebElement proceedButtonToCC;
	
	@FindBy(xpath="//input[@type='text']")
	WebElement searchNameInputBox;
	
	@FindBy(xpath="//div[@class='ReactVirtualized__Grid ReactVirtualized__Table__Grid']//div[@class='ReactVirtualized__Table__rowColumn'][1]")
	List<WebElement> namePAN;
	
	@FindBy(xpath="//div[@class='ReactVirtualized__Grid ReactVirtualized__Table__Grid']//div[@class='ReactVirtualized__Table__rowColumn'][3]")
	List<WebElement> clientStatus;
	
			
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public int getClientCount( ) throws InterruptedException {
		
		TestUtility.clickOn(driver, selectRM,3);
		TestUtility.clickOn(driver, adminLogin, 3);
		TestUtility.clickOn(driver, confirmRM, 5);
		Thread.sleep(Constants.MEDIUM_WAIT);
		TestUtility.clickOn(driver, filterByStatus, 3);
		TestUtility.clickOn(driver, approvalPendingByClientStatus, 3);
		TestUtility.clickOn(driver, filterByStatus, 3);
		return clientList.size();
						
	}
	
	public int getTotalUCC() {
		int uccCount =  Integer.parseInt(totalUCC.getText().split(":")[1].trim());    
		return uccCount;
		
	}
	
	public ClientCreationPage navigateToClientCreation(String taxStatus,String holdingStatus) {
		
		TestUtility.clickOn(driver, createClientButton,3);
		
		TestUtility.clickOn(driver, taxStatusDropDown,3);
		
		taxStatusListBox.findElement(By.xpath("//li[text()='"+ taxStatus +"']")).click();
		if(!(taxStatus.equals("Company")|| taxStatus.equals("Trust"))) {		
		TestUtility.clickOn(driver, holdingStatusDropDown,3);
		}
		
		if(!(taxStatus.equals("On Behalf Of Minor")|| taxStatus.equals("Company") || taxStatus.equals("Trust"))) {
			taxStatusListBox.findElement(By.xpath("//li[text()='"+ holdingStatus +"']")).click();
			
		}
		
		TestUtility.clickOn(driver, proceedButtonToCC,3);
		
		return new ClientCreationPage();
		
	}
	
	
	public String searchForClient(String clientName) {
		
		TestUtility.sendKeys(driver, searchNameInputBox, 3, clientName);
		
		TestUtility.pressEnterKey(searchNameInputBox);
		
		String nameOfClient = TestUtility.getDataFromClientSearchList(clientName);
		
		return nameOfClient;	
	}
	
	
public String getClientSubmissionStatus(String accountSubmissionstatus) {
		
		
		String stat = TestUtility.getDataFromClientSearchList(accountSubmissionstatus);
		
		return stat;	
	}
	

public List<String> getColumnsList() {
	
	List<String> colList  = new ArrayList<>(columnsList.size());
	
	for(WebElement e : columnsList) {
	
		colList.add(e.getText());
		
	}
	return colList;	
	
}


public void switchLogin() {
	
	TestUtility.clickOn(driver, loggedInUser, 3);
	TestUtility.clickOn(driver, selectRM,3);
	TestUtility.clickOn(driver, selfRM, 3);
	TestUtility.clickOn(driver, confirmRM, 3);
	TestUtility.pageRefresh(driver);
}
	
	
}
