package com.pwm.Pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.github.javafaker.Faker;
import com.pwm.BaseClass.TestBase;
import com.pwm.utilities.TestUtility;

import Constants.Constants;

public class ClientCreationPage extends TestBase {

	@FindBy(id="panBasicInfo0") // check required for 4th character(should be P for indivdual)
	WebElement panInfo;
	
	@FindBy(id="guardian_panBasicInfo0") // check required for 4th character(should be P for indivdual)
	WebElement guardianPanInfo;
	
	
	@FindBy(id="nameBasicInfo0")
	WebElement applicantName;
	
	@FindBy(id="date_of_incorporationBasicInfo0")
	WebElement dateOfIncorporationInfo;
	
	@FindBy(id="guardian_nameBasicInfo0")
	WebElement guardianName;
	
	@FindBy(id="cp_nameBasicInfo0") 
	WebElement contactPersonName;
	
	@FindBy(id="networthKycRequirements") 
	WebElement netWorth;
	
	@FindBy(id="networth_dateKycRequirements") 
	WebElement nwAsOnDate;
	
	@FindBy(id="mui-component-select-corp_servs_code") 
	WebElement corpSector;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']/li[1]") 
	WebElement corpSectorDropDown;
	
	@FindBy(id="dobBasicInfo0")
	WebElement dobInfo;
	
	@FindBy(id="guardian_dobBasicInfo0")
	WebElement guardianDOBInfo;
	
	@FindBy(id="mui-component-select-non_finan_entity_cat")
	WebElement nonFinEntity;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']/li[3]") 
	WebElement nonFinEntityDropDown;
	
	@FindBy(id="place_of_birthBasicInfo0")
	WebElement placeOfBirthInfo;
	
	@FindBy(id="mui-component-select-gender") 
	WebElement genderInfo;
	
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //gender info dropdown
	WebElement genderInfoDropDown;
	
	@FindBy(id="mobileBasicInfo0") //should be of 10 digits - check required
	WebElement mobileInfo;
	
	@FindBy(id="emailBasicInfo0") //proper format - check required
	WebElement emailInfo;
	
	@FindBy(id="mui-component-select-wealth_source_code") 
	WebElement wealthSourceInfo;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //wealth source dropdown
	WebElement wealthSourceDropDown;
	
	
	@FindBy(id="mui-component-select-income_slab") //income slab dropdown
	WebElement incomeSlabInfo;
	
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //income Slab dropdown
	WebElement incomeSlabDropDown;
	
	@FindBy(id="mui-component-select-politically_exposed") 
	WebElement polInfo;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //polinfo dropdown
	WebElement polInfoDropDown;
	
	@FindBy(id="mui-component-select-occupation_code") 
	WebElement occCode;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //occCode dropdown
	WebElement occCodeDropDown;
	
	@FindBy(id="mui-component-select-countryCode") 
	WebElement countryOfBirth;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //countryofBirth dropdown
	WebElement countryOfBirthDropDown;
	
	@FindBy(id="mui-component-select-address_type") 
	List<WebElement> addressTypeOfKRA;
	
	@FindBy(id="place_incorpFatcaAndCrs0") 
	WebElement incorpPlace;
	
	@FindBy(id="mui-component-select-countryCode") 
	WebElement countryOfIncorporation;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //countryOfIncorporation dropdown
	WebElement countryOfIncorporationDropDown;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //addressTypeOfKRA dropdown
	WebElement addressTypeOfKRADropDown;
	
	@FindBy(id="mui-component-select-kyc_type_code") 
	WebElement kycType;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //kycType dropdown
	WebElement kycTypeDropDown;
	
	
	@FindBy(id="mui-component-select-taxResidentothIn") 
	List<WebElement> taxResidentOutsideCountry;
	
	@FindBy(id="mui-component-select-ffi_drnfe") 
	WebElement ffiDrnfe;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']/li[1]") 
	WebElement ffiDrnfeDropDown;
	
	@FindBy(id="ubo_nameUboDetails0") 
	WebElement uboName;
	
	@FindBy(id="ubo_panUboDetails0") 
	WebElement uboPAN;
	
	@FindBy(id="mui-component-select-ubo_code") 
	WebElement uboCode;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']/li[1]") 
	WebElement uboCodeDropDown;
	
	@FindBy(id="mui-component-select-country_birth") 
	WebElement countryBirth;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") 
	WebElement countryBirthDropDown;
	
	@FindBy(id="mui-component-select-nationality") 
	WebElement nationality;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") 
	WebElement nationalityDropDown;
	
	
	@FindBy(id="address1UboDetails0") 
	WebElement addressLine1Ubo;
	
	@FindBy(id="address2UboDetails0") 
	WebElement addressLine2Ubo;
	
	@FindBy(id="address3UboDetails0") 
	WebElement addressLine3Ubo;
	
	@FindBy(id="pincodeUboDetails0") 
	WebElement pinCodeUbo;
	
	@FindBy(id="cityUboDetails0") 
	WebElement cityUbo;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //taxResidentOutsideCountry dropdown
	WebElement taxResidentOutsideCountryDropDown;
	
	@FindBy(id="mui-component-select-isNominee") 
	WebElement nomineeInfo;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //nomineeInfo dropdown
	WebElement nomineeInfoDropDown;
	
	@FindBy(id="nominee_nameNomineeDetails0") 
	WebElement nomineeName;
	
	@FindBy(id="nominee_relationNomineeDetails0") 
	WebElement relationNominee;
	
	
	@FindBy(id="nominee_shareNomineeDetails0") 
	WebElement nomineeShare;
	
	
	@FindBy(id="is_nominee_minorNomineeDetails0") 
	WebElement isMinor;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //isMinor dropdown
	WebElement isMinorDropDown;
	
	@FindBy(id="mui-component-select-account_type") 
	WebElement accountType;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //accountType dropdown
	WebElement accountTypeDropDown;
	
	@FindBy(id="account_numberBankInfo0") 
	WebElement accountNumber;
	
	@FindBy(id="ifsc_codeBankInfo0")  //Check required for 11 alphanumeric
	WebElement ifscCode;
	
	@FindBy(id="bank_nameBankInfo0")  
	WebElement bankName;
	
	@FindBy(id="branch_nameBankInfo0")  
	WebElement branchName;
	
	@FindBy(id="mui-component-select-default_bank") 
	WebElement defaultBank;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //defaultBank dropdown
	WebElement defaultBankDropDown;
	
	@FindBy(id="address1CommAdd") 
	WebElement addressLine1;
	
	
	@FindBy(id="pincodeCommAdd") 
	WebElement pincode;
	

	@FindBy(id="cityCommAdd") 
	WebElement city;
	
	@FindBy(id="mui-component-select-state") 
	List<WebElement> state;
	
	@FindBy(xpath="//ul[@class='MuiList-root MuiMenu-list MuiList-padding']") //state dropdown
	WebElement stateDropDown;
	
	@FindBy(xpath="//button[text()='SAVE DATA']") 
	WebElement saveDataButton;
	
	@FindBy(xpath="//span[text()='Send pre-filled forms to client on email']") 
	WebElement sendFormToClientLink;
	
	@FindBy(xpath="//button[text()='Ok']") 
	WebElement acceptOnModalPopUp;
	
	@FindBy(xpath="//h3[text()='STEP 2: UPLOAD FORM & DOCUMENTS']") 
	WebElement verifyStep2StepText;
	
	@FindBy(xpath="//div[text()='Account Opening Form']/following-sibling::label") 
	WebElement accountOpeningFormUpload;
	
	@FindBy(xpath="//div[text()='PAN Card']/following-sibling::label") 
	WebElement PANCardFormUpload;
		
	@FindBy(xpath="//div[text()='Board Resolution']/following-sibling::label") 
	WebElement boardResolutionFormUpload;
		
	@FindBy(xpath="//div[text()='Date of Birth Proof']/following-sibling::label") 
	WebElement dateOfBirthProofFormUpload;
	
	@FindBy(xpath="//div[text()='Bank Proof']/following-sibling::label") 
	WebElement bankProofFormUpload;
	
	@FindBy(xpath="//button[text()='Submit']") 
	WebElement submitButton;
	
	@FindBy(xpath="//div[text()='OPTION 1: UPLOAD SIGNED FORM & DOCUMENTS']") 
	WebElement uploadSignedFormAccordion;
	
	@FindBy(xpath="//div[text()='UPLOAD SIGNED FORM & DOCUMENTS']") 
	WebElement uploadSignedFormNonIndividualAccordion;
	
	Faker faker = new Faker();
	
	Robot robot;
	
	private static String fullName;
	
	public ClientCreationPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public HomePage formFillUpForIndividualClient(String taxStatus,String holdingStatus) throws AWTException, InterruptedException {
		
//	String panCardNo = Constants.INDIVIDUAL_PAN_NO;
		/* This will generate a random PAN no. */
		
		if(!(taxStatus.equals("On Behalf Of Minor"))) {
			String panCardNo = faker.regexify("[A-Z]{3}[P][A-Z]{1}[0-9]{4}[A-Z]{1}");		
	    TestUtility.sendKeys(driver, panInfo, 3, panCardNo);
		}
	
	/* This will generate a random name */
	
	fullName = faker.name().fullName();
	
	TestUtility.sendKeys(driver, applicantName, 3, fullName);
	
	/* This will generate a random date between 18 years and 65 years */
	
	if(taxStatus.equals("On Behalf Of Minor")) {
		String dob = new SimpleDateFormat("ddMMyyyy").format(faker.date().birthday(2, 15));
		TestUtility.sendKeys(driver, dobInfo, 3, dob);
	}
	else {
		String dob = new SimpleDateFormat("ddMMyyyy").format(faker.date().birthday(20, 65));
		TestUtility.sendKeys(driver, dobInfo, 3, dob);
	}
	
		
	/* This will generate a random city name */
	
	String placeOfBirth = faker.address().city();
	
	TestUtility.sendKeys(driver, placeOfBirthInfo, 3, placeOfBirth);
	
	TestUtility.clickOn(driver, genderInfo,3);
	
	genderInfoDropDown.findElement(By.xpath("//li[text()='Male']")).click();
	
	/* This will generate a random phone number */ 
	String phoneNo = faker.number().digits(10);
	TestUtility.sendKeys(driver, mobileInfo, 3, phoneNo);
	
	if(taxStatus.equals("On Behalf Of Minor")) {
		String guardianPanCardNo = faker.regexify("[A-Z]{3}[P][A-Z]{1}[0-9]{4}[A-Z]{1}");
		String guardiandob = new SimpleDateFormat("ddMMyyyy").format(faker.date().birthday(18, 65));
		String guardname = faker.name().fullName();
		TestUtility.sendKeys(driver, guardianName, 3, guardname);
		Thread.sleep(Constants.SHORT_WAIT);
		TestUtility.sendKeys(driver, guardianPanInfo, 3, guardianPanCardNo);
		Thread.sleep(Constants.SHORT_WAIT);
		TestUtility.sendKeys(driver, guardianDOBInfo, 3, guardiandob);
	}
	
	/* This will generate a random email */
	String emailID =faker.internet().emailAddress();
	
	//String emailID = faker.regexify("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$");
	TestUtility.sendKeys(driver, emailInfo, 3, emailID);
	
	TestUtility.clickOn(driver, wealthSourceInfo,3);
	
	Thread.sleep(Constants.SHORT_WAIT);
	
	wealthSourceDropDown.findElement(By.xpath("//li[text()='Salary']")).click(); //TBD after application is up
	
    TestUtility.clickOn(driver, incomeSlabInfo,3);
    Thread.sleep(Constants.SHORT_WAIT);
	
    incomeSlabDropDown.findElement(By.xpath("//li[text()='> 1 <=5 Lacs']")).click(); //TBD after application is up
    
    TestUtility.clickOn(driver, polInfo,3);
    Thread.sleep(Constants.SHORT_WAIT);
	
    polInfoDropDown.findElement(By.xpath("//li[text()='No']")).click();  //TBD after application is up
	
    TestUtility.clickOn(driver, occCode,3);
    Thread.sleep(Constants.SHORT_WAIT);
	
    occCodeDropDown.findElement(By.xpath("//li[text()='Business']")).click(); //TBD after application is up
	
    TestUtility.clickOn(driver, countryOfBirth,3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    countryOfBirthDropDown.findElement(By.xpath("//li[text()='Argentina']")).click(); //TBD after application is up
		
    TestUtility.clickOn(driver, addressTypeOfKRA.get(0),3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    addressTypeOfKRADropDown.findElement(By.xpath("//li[text()='Residential or Business']")).click(); //TBD after application is up
	
    TestUtility.clickOn(driver, kycType,3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    kycTypeDropDown.findElement(By.xpath("//li[text()='KRA Compliant']")).click(); //TBD after application is up
    
    TestUtility.clickOn(driver, taxResidentOutsideCountry.get(0),3);
	
    taxResidentOutsideCountryDropDown.findElement(By.xpath("//li[text()='No']")).click(); //TBD after application is up
    
    if(!(taxStatus.equals("On Behalf Of Minor"))) {
    
    TestUtility.clickOn(driver, nomineeInfo,3);
	
    nomineeInfoDropDown.findElement(By.xpath("//li[text()='Yes']")).click(); //TBD after application is up
	
    
    /* This will generate a random name */
	
	String nomineefullName = faker.name().fullName();
    
	TestUtility.sendKeys(driver, nomineeName, 3, nomineefullName);
	
	/* This will generate a random relationship */
	String relationship = faker.relationships().any();
	
	TestUtility.sendKeys(driver, relationNominee, 3, relationship);
	
	TestUtility.sendKeys(driver, nomineeShare, 3, "100");
	
    TestUtility.clickOn(driver, isMinor,3);
    
    }
	
   // isMinorDropDown.findElement(By.xpath("//li[text()='No']")).click(); //TBD after application is up
    
    TestUtility.scrollDownPageByJavaScript(driver);
    
    TestUtility.clickOn(driver, accountType,3);
	
    accountTypeDropDown.findElement(By.xpath("//li[text()='Saving Bank']")).click(); //TBD after application is up
    
    /* This will generate a random account number */
    String accNumber = faker.number().digits(11);
    
    TestUtility.sendKeys(driver, accountNumber, 3, accNumber);
    
    
    String ifscCo = Constants.IFSC_CODE;
	
    TestUtility.sendKeys(driver, ifscCode, 3, ifscCo);
	
    TestUtility.sendKeys(driver, bankName, 3, "HDFC");
	
    TestUtility.sendKeys(driver, branchName, 3, "delhi");
	
    TestUtility.clickOn(driver, defaultBank,3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    defaultBankDropDown.findElement(By.xpath("//li[text()='Yes']")).click(); //TBD after application is up
    
    String addrLine1 = faker.address().buildingNumber();
    
    TestUtility.sendKeys(driver, addressLine1, 3, addrLine1);
	
    String pinCo = faker.number().digits(6);
    
    TestUtility.sendKeys(driver, pincode, 3, pinCo);
    
    String cityName = faker.address().city();
    
    TestUtility.sendKeys(driver, city, 3, cityName);
    
    TestUtility.clickOn(driver, state.get(0),3);
    
    Thread.sleep(Constants.SHORT_WAIT);
    
    TestUtility.scrollDownPageByJavaScript(driver);
	
    stateDropDown.findElement(By.xpath("//li[text()='New Delhi']")).click(); //TBD after application is up
    
    TestUtility.clickOn(driver, saveDataButton,3);
    
    Thread.sleep(Constants.MEDIUM_WAIT);
    
    TestUtility.clickOn(driver, uploadSignedFormAccordion,3);
        
    TestUtility.clickOn(driver, sendFormToClientLink,3);
    
    TestUtility.clickOn(driver, acceptOnModalPopUp,6);
        
    TestUtility.clickOn(driver, accountOpeningFormUpload,3);
    
    TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");
    
    Thread.sleep(Constants.MEDIUM_WAIT);
    
    if(taxStatus.equals("On Behalf Of Minor")) {
    	
    	TestUtility.clickOn(driver, dateOfBirthProofFormUpload,3);	
    	TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");
    	Thread.sleep(Constants.MEDIUM_WAIT);
    }
        
    //TestUtility.sendKeys(driver, accountOpeningFormUpload, 3, "/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");
    
    TestUtility.clickOn(driver, bankProofFormUpload,3);
	/*
	 * robot= new Robot(); robot.keyPress(KeyEvent.VK_ENTER);
	 * robot.keyRelease(KeyEvent.VK_ENTER);
	 */
    
    TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");
	Thread.sleep(Constants.MEDIUM_WAIT);
    
    TestUtility.scrollIntoElementByJavaScript(submitButton, driver);    
    TestUtility.clickOn(driver, submitButton,3);
    Thread.sleep(Constants.MEDIUM_WAIT);
    
    TestUtility.pageRefresh(driver);
    Thread.sleep(Constants.MEDIUM_WAIT);
    
    return new HomePage();
    
       
	
	}
	
	public HomePage formFillUpForNonIndividualClient(String taxStatus) throws InterruptedException, AWTException {
		
		if(taxStatus.equals("Company")) {
		String panCardNo = faker.regexify("[A-Z]{3}[C][A-Z]{1}[0-9]{4}[A-Z]{1}");		
	    TestUtility.sendKeys(driver, panInfo, 3, panCardNo);
	    }
		
		else {
			String panCardNo = faker.regexify("[A-Z]{3}[T][A-Z]{1}[0-9]{4}[A-Z]{1}");		
		    TestUtility.sendKeys(driver, panInfo, 3, panCardNo);
			
		}

/* This will generate a random name */
	
	fullName = faker.name().fullName();
	
	TestUtility.sendKeys(driver, applicantName, 3, fullName);
	Thread.sleep(Constants.SHORT_WAIT);

String doi = new SimpleDateFormat("ddMMyyyy").format(faker.date().birthday(18, 50));
		TestUtility.sendKeys(driver, dateOfIncorporationInfo, 3, doi);
		Thread.sleep(Constants.SHORT_WAIT);

/* This will generate a random phone number */ 
	String phoneNo = faker.number().digits(10);
	TestUtility.sendKeys(driver, mobileInfo, 3, phoneNo);


/* This will generate a random email */
	String emailID =faker.internet().emailAddress();
	
	//String emailID = faker.regexify("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$");
	TestUtility.sendKeys(driver, emailInfo, 3, emailID);


String cpfName = faker.name().fullName();
	
	TestUtility.sendKeys(driver, contactPersonName, 3, cpfName);


TestUtility.clickOn(driver, wealthSourceInfo,3);
	
	Thread.sleep(Constants.SHORT_WAIT);
	
	wealthSourceDropDown.findElement(By.xpath("//li[text()='Salary']")).click(); //TBD after application is up
	
    TestUtility.clickOn(driver, incomeSlabInfo,3);
    Thread.sleep(Constants.SHORT_WAIT);
	
    incomeSlabDropDown.findElement(By.xpath("//li[text()='> 1 <=5 Lacs']")).click(); //TBD after application is up
    
    
TestUtility.sendKeys(driver, netWorth, 3, "99999");

String nwDate = new SimpleDateFormat("ddMMyyyy").format(faker.date().birthday(0, 1));
		TestUtility.sendKeys(driver, nwAsOnDate, 3, nwDate);


TestUtility.clickOn(driver, corpSector,3);
		Thread.sleep(Constants.SHORT_WAIT);
		TestUtility.clickOn(driver, corpSectorDropDown,3);


TestUtility.clickOn(driver, occCode,3);
    Thread.sleep(Constants.SHORT_WAIT);
	
    occCodeDropDown.findElement(By.xpath("//li[text()='Business']")).click(); //TBD after application is up


TestUtility.clickOn(driver, polInfo,3);
    Thread.sleep(Constants.SHORT_WAIT);
	
    polInfoDropDown.findElement(By.xpath("//li[text()='No']")).click();  //TBD after application is up


TestUtility.clickOn(driver, addressTypeOfKRA.get(0),3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    addressTypeOfKRADropDown.findElement(By.xpath("//li[text()='Residential or Business']")).click(); //TBD after application is up


TestUtility.clickOn(driver, kycType,3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    kycTypeDropDown.findElement(By.xpath("//li[text()='KRA Compliant']")).click(); //TBD after application is up
    

String poi=faker.address().city();
	    TestUtility.sendKeys(driver, incorpPlace, 3, poi);

TestUtility.clickOn(driver, countryOfBirth,3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    countryOfBirthDropDown.findElement(By.xpath("//li[text()='Argentina']")).click(); //TBD after application is up
  
    TestUtility.clickOn(driver, taxResidentOutsideCountry.get(0),3);
	
    taxResidentOutsideCountryDropDown.findElement(By.xpath("//li[text()='No']")).click(); //TBD after application is up


TestUtility.clickOn(driver, ffiDrnfe,3);
		Thread.sleep(Constants.SHORT_WAIT);
		TestUtility.clickOn(driver, ffiDrnfeDropDown,3);

		TestUtility.clickOn(driver, nonFinEntity,3);
		Thread.sleep(Constants.SHORT_WAIT);
		TestUtility.clickOn(driver, nonFinEntityDropDown,3);

String uboN = faker.name().fullName();
		TestUtility.sendKeys(driver, uboName, 3, uboN);
		String uboP = faker.regexify("[A-Z]{3}[P][A-Z]{1}[0-9]{4}[A-Z]{1}");
		TestUtility.sendKeys(driver, uboPAN, 3, uboP);
		TestUtility.clickOn(driver, uboCode,3);
		Thread.sleep(Constants.SHORT_WAIT);
		TestUtility.clickOn(driver, uboCodeDropDown,3);


TestUtility.clickOn(driver, countryBirth,3);
		Thread.sleep(Constants.SHORT_WAIT);
		countryBirthDropDown.findElement(By.xpath("//li[text()='Argentina']")).click();
		TestUtility.clickOn(driver, nationality,3);
		Thread.sleep(Constants.SHORT_WAIT);
		nationalityDropDown.findElement(By.xpath("//li[text()='India']")).click();
		String uboAdddr1 = faker.address().buildingNumber();
		TestUtility.sendKeys(driver, addressLine1Ubo, 3, uboAdddr1);
		String uboAdddr2 = faker.address().streetName();
		TestUtility.sendKeys(driver, addressLine2Ubo, 3, uboAdddr2);
		String uboAdddr3 = faker.address().streetAddressNumber();
		TestUtility.sendKeys(driver, addressLine3Ubo, 3, uboAdddr3);


String uboPIN = faker.number().digits(6);
		TestUtility.sendKeys(driver, pinCodeUbo, 3, uboPIN);
		String uboCi = faker.address().city();


TestUtility.sendKeys(driver, cityUbo, 3, uboCi);

 TestUtility.clickOn(driver, state.get(0),3);
    
    Thread.sleep(Constants.SHORT_WAIT);
    
    TestUtility.scrollDownPageByJavaScript(driver);
	
    stateDropDown.findElement(By.xpath("//li[text()='Delhi']")).click(); //TBD after application is up


TestUtility.clickOn(driver, addressTypeOfKRA.get(1),3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    addressTypeOfKRADropDown.findElement(By.xpath("//li[text()='Residential or Business']")).click(); //TBD after application is up


TestUtility.clickOn(driver, taxResidentOutsideCountry.get(1),3);
	Thread.sleep(Constants.SHORT_WAIT);
    taxResidentOutsideCountryDropDown.findElement(By.xpath("//li[text()='No']")).click(); //TBD after application is up
    

TestUtility.clickOn(driver, accountType,3);
	
    accountTypeDropDown.findElement(By.xpath("//li[text()='Current Bank']")).click(); //TBD after application is up
    
    /* This will generate a random account number */
    String accNumber = faker.number().digits(11);
    
    TestUtility.sendKeys(driver, accountNumber, 3, accNumber);
    
    
    String ifscCo = Constants.IFSC_CODE;
	
    TestUtility.sendKeys(driver, ifscCode, 3, ifscCo);
	
    TestUtility.sendKeys(driver, bankName, 3, "HDFC");
	
    TestUtility.sendKeys(driver, branchName, 3, "delhi");
	
    TestUtility.clickOn(driver, defaultBank,3);
    
    Thread.sleep(Constants.SHORT_WAIT);
	
    defaultBankDropDown.findElement(By.xpath("//li[text()='Yes']")).click(); //TBD after application is up

String addrLine1 = faker.address().buildingNumber();
    
    TestUtility.sendKeys(driver, addressLine1, 3, addrLine1);

String pinCo = faker.number().digits(6);
    
    TestUtility.sendKeys(driver, pincode, 3, pinCo);
    
    String cityName = faker.address().city();
    
    TestUtility.sendKeys(driver, city, 3, cityName);
    
    TestUtility.clickOn(driver, state.get(1),3);
    
    Thread.sleep(Constants.SHORT_WAIT);
    
    TestUtility.scrollDownPageByJavaScript(driver);
	
    stateDropDown.findElement(By.xpath("//li[text()='New Delhi']")).click(); //TBD after application is up

TestUtility.clickOn(driver, saveDataButton,3);
    
Thread.sleep(Constants.MEDIUM_WAIT);

TestUtility.clickOn(driver, uploadSignedFormNonIndividualAccordion,3);
    
TestUtility.clickOn(driver, sendFormToClientLink,3);

TestUtility.clickOn(driver, acceptOnModalPopUp,6);
    
TestUtility.clickOn(driver, accountOpeningFormUpload,3);

TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");

Thread.sleep(Constants.MEDIUM_WAIT);

if(taxStatus.equals("Trust")) {

TestUtility.clickOn(driver, PANCardFormUpload,3);

TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");

Thread.sleep(Constants.MEDIUM_WAIT);

}

TestUtility.clickOn(driver, boardResolutionFormUpload,3);

TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");

Thread.sleep(Constants.MEDIUM_WAIT);

TestUtility.clickOn(driver, bankProofFormUpload,3);

TestUtility.uploadFiles("/home/shaktiganesh/git/pwmautomation1/src/main/java/com/pwm/Configuration/21188A6551.pdf");
Thread.sleep(Constants.MEDIUM_WAIT);

TestUtility.scrollIntoElementByJavaScript(submitButton, driver);    
TestUtility.clickOn(driver, submitButton,3);
Thread.sleep(Constants.MEDIUM_WAIT);

TestUtility.pageRefresh(driver);
Thread.sleep(Constants.MEDIUM_WAIT);

return new HomePage();
		
	}
	
	
	public String getClientName() {
		
		
	return fullName;	
		
		
	}
	
	
	
	
	
	
	
	
}
