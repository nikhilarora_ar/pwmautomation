package com.pwm.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Constants.Constants;
import com.pwm.BaseClass.TestBase;

/*import com.crm.qa.BaseClass.TestBase;*/

public class LoginPage extends TestBase 
{
	@FindBy(name="employeeId")
	WebElement username;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(xpath="//button[text()='USER LOGIN']") 
	WebElement loginButton;
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public String validateLoginPageTitle()
	{
		return driver.getTitle();
	}
	
	
	public HomePage login(String uname, String pword) throws InterruptedException
	{
		username.sendKeys(uname);
		password.sendKeys(pword);
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", loginButton);
		
		Thread.sleep(Constants.MEDIUM_WAIT);
		return new HomePage();
	}
}

