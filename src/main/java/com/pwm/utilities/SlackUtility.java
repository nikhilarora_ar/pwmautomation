package com.pwm.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;

import com.github.seratch.jslack.Slack;

import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;


public class SlackUtility {

	private static String urlSlackWebHook = "https://hooks.slack.com/services/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";
	private static String channelName = "slackingexperiments";
	private static String botUserOAuthAccessToken = "xoxb-752569668147-2239376834741-er2EOwlTc4yB72jgQFf3zm34";

	public void sendTestExecutionStatusToSlack(String message) throws Exception {
	try {
	StringBuilder messageBuider = new StringBuilder();
	messageBuider.append(message);
	@SuppressWarnings("deprecation")
	Payload payload = Payload.builder().channel(channelName).text(messageBuider.toString()).build();

	WebhookResponse webhookResponse = Slack.getInstance().send(urlSlackWebHook, payload);
	webhookResponse.getMessage();
	} catch (IOException e) {
	System.out.println("Unexpected Error! WebHook:" + urlSlackWebHook);
	}
	}

	public void sendTestExecutionReportToSlack(String testReportPath) throws Exception {
		
		String url = "https://slack.com/api/files.upload";
		try {
			HttpClient httpclient = HttpClientBuilder.create().disableContentCompression().build();
			HttpPost httppost = new HttpPost(url);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			FileBody fileBody = new FileBody(new File(testReportPath));
			builder.addPart("file", fileBody);
			builder.addTextBody("channels", channelName);
			builder.addTextBody("token",botUserOAuthAccessToken);
			//builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			/*
			 * FileBody fileBody = new FileBody(new File(testReportPath));
			 * builder.addPart("file", fileBody);
			 */
			httppost.setEntity(builder.build());
			HttpResponse response = null;
			response = httpclient.execute(httppost);
			HttpEntity result = response.getEntity();
			System.out.println(result.toString());
			System.out.println(response.getStatusLine().getReasonPhrase());
		    System.out.println(response.getStatusLine().getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
