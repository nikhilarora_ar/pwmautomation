package TestListeners;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.pwm.utilities.SlackUtility;
import com.pwm.utilities.TestUtility;

public class ExtentReportListener implements ITestListener
{
	
	SlackUtility slackutil = new SlackUtility();
	int successCount = 0, failureCount = 0, skippedCount = 0, testCount = 0;
	public static ExtentSparkReporter sparkReporter;
	public static ExtentReports extentReport;
	String reportPath = System.getProperty("user.dir") + "/AutomationReport/";
	public static ExtentTest extentTest;
	public static WebDriver driver;
	
	public void onTestStart(ITestResult result) {
		String className = this.getClass().getSimpleName();
		String testName = result.getMethod().getMethodName();
		extentTest = extentReport.createTest(className + "-" + testName);
	}

	public void onTestSuccess(ITestResult result) {
		successCount++;
		String testName = result.getMethod().getMethodName();
		String testPassMessage = testName + " is PASSED";
		try {
			// Send test results to slack channel
			slackutil.sendTestExecutionStatusToSlack(testPassMessage);

			extentTest.log(Status.PASS,
					MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
			extentTest.pass(testName + "Test Step Passed");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onTestFailure(ITestResult result) {
		failureCount++;
		String testName = result.getMethod().getMethodName();
		String testPassMessage = testName + " is FAILED";

		Throwable throwable = null;

		try {
			throwable = result.getThrowable().getCause();
		} catch (Exception e) {

		}
		if (throwable == null) {
			throwable = result.getThrowable();
		}

		try {
			slackutil.sendTestExecutionStatusToSlack(testPassMessage);
			if (throwable.toString().length() >= 145) {
				slackutil.sendTestExecutionStatusToSlack(
						"REASON FOR FAIL " + testName + " : " + throwable.toString().substring(0, 143));
			} else {
				slackutil.sendTestExecutionStatusToSlack(
						"REASON FOR FAIL " + testName + " : " + throwable.toString().split(":")[1].trim());
			}

			extentTest.log(Status.FAIL, MarkupHelper.createLabel(testName + " - Test Case Failed", ExtentColor.RED));
			extentTest.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			//extentTest.addScreenCaptureFromPath(TestUtility.getScreenshot(driver, result.getMethod().getMethodName()));
			extentTest.fail(testName + "Test Step Failed");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onTestSkipped(ITestResult result) {
		skippedCount++;
		String testName = result.getMethod().getMethodName();
		String testPassMessage = testName + " is SKIPPED";
		try {
			// Send test results to slack channel
			slackutil.sendTestExecutionStatusToSlack(testPassMessage);

			String className = this.getClass().getSimpleName();
			extentTest = extentReport.createTest(className + "-" + testName);
			extentTest.createNode(testName);
			extentTest.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
			extentTest.skip(testName + "Test Step Skipped");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) 
	{

	}

	public void onStart(ITestContext testContext) {
		try {
			sparkReporter = new ExtentSparkReporter(reportPath);
			sparkReporter.config().setDocumentTitle("PWM Automation");
			sparkReporter.config().setReportName("Automation Execution Report");
			sparkReporter.config().setTheme(com.aventstack.extentreports.reporter.configuration.Theme.DARK);
			extentReport = new ExtentReports();
			extentReport.attachReporter(sparkReporter);
			extentReport.setSystemInfo("Application Name", "ExtentReport");
			extentReport.setSystemInfo("Platform", System.getProperty("os.name"));
			extentReport.setSystemInfo("Environment", "QA");
			slackutil.sendTestExecutionStatusToSlack("Test Case Execution Started...");
			slackutil.sendTestExecutionStatusToSlack(
					"=========================TEST CASES STATUS==================================");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFinish(ITestContext testContext) {
		int totalcount = successCount + failureCount + skippedCount;
		try {
			extentReport.flush();
			DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH-mm-ss");
			Date date = new Date();
			String filePathdate = dateFormat.format(date).toString();
			String actualReportPath = reportPath + "index.html";
			new File(actualReportPath).renameTo(new File(System.getProperty("user.dir") + "/AutomationReport/"
					+ "Automation_Report_" + filePathdate + ".html"));
			
			slackutil.sendTestExecutionStatusToSlack(
					"=========================TEST EXECUTION SUMMARY===========================");
			slackutil.sendTestExecutionStatusToSlack(
					"TOTAL TESTS : " + totalcount + "\n" + "PASSED TESTS : " + successCount + "\n" + "FAILED TESTS : "
							+ failureCount + "\n" + "SKIPPED TESTS : " + skippedCount);
			slackutil.sendTestExecutionReportToSlack(System.getProperty("user.dir") + "/AutomationReport/"
					+ "Automation_Report_" + filePathdate + ".html");
			slackutil.sendTestExecutionStatusToSlack(
					"=========================WAIT TO BEGIN AN ANOTHER EXECUTION================");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
