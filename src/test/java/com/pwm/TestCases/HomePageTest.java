package com.pwm.TestCases;

import java.awt.AWTException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import TestListeners.ExtentReportListener;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.pwm.BaseClass.TestBase;
import com.pwm.Pages.ClientCreationPage;
import com.pwm.Pages.HomePage;
import Constants.Constants;
import com.pwm.Pages.LoginPage;

@Listeners(TestListeners.ExtentReportListener.class)
public class HomePageTest extends TestBase {
	LoginPage loginPage;
	HomePage homePage;
	ClientCreationPage clientCreationPage;

	public HomePageTest() {
		super();
	}

	@BeforeClass(alwaysRun = true)
	public void setUp() {
		initialization();
		Log.info("Application Launched Successfully");

		loginPage = new LoginPage();
	}

	@Test(priority = 1, enabled = true)
	public void verifyTotalUCCBasedOnStatusFilter() throws InterruptedException {
		homePage = loginPage.login(property.getProperty("Username"), property.getProperty("Password"));
		int expUCCCount = homePage.getClientCount();
		int actualUCCCount = homePage.getTotalUCC();

		Assert.assertEquals(expUCCCount, actualUCCCount, "UCC count after filtering is not matching");
	}
	
	@Test(priority = 3, enabled = true)
	public void verifyColumnsForRMLogin() {
		
		homePage.switchLogin();
		
		List<String> colList = homePage.getColumnsList();
		
		Assert .assertEquals(colList.size(), 3, "Number of columns does not match");
		Assert.assertEquals(colList.get(0), "NAME /PAN","Column 1 does not match");
		Assert.assertEquals(colList.get(1), "HOLDING NATURE / CLIENT CODE (D/P)","Column 2 does not match");
		Assert.assertEquals(colList.get(2), "STATUS","Column 3 does not match");
	}
	
	@Test(priority = 2, enabled = true)
	public void verifyColumnsForAdminLogin() {
		
		List<String> colList = homePage.getColumnsList();
		
		Assert .assertEquals(colList.size(), 4, "Number of columns does not match");
		Assert.assertEquals(colList.get(0), "NAME /PAN","Column 1 does not match");
		Assert.assertEquals(colList.get(1), "HOLDING NATURE / CLIENT CODE (D/P)","Column 2 does not match");
		Assert.assertEquals(colList.get(2), "RM CODE (RM Name)","Column 3 does not match");
		Assert.assertEquals(colList.get(3), "STATUS","Column 4 does not match");
	}

	
	
	
	  @Test(priority=4, dataProvider="getIndividualData",enabled=true) public void
	  verifyIndividualClientCreation(String taxStatus,String holdingStatus) throws
	  AWTException, InterruptedException {
	  
	  ExtentReportListener.extentTest.info(MarkupHelper.
	  createLabel("Starting client creation for  " +taxStatus+ "",
	  ExtentColor.GREEN));
	  
	  clientCreationPage = homePage.navigateToClientCreation(taxStatus,
	  holdingStatus);
	  
	  homePage = clientCreationPage.formFillUpForIndividualClient(taxStatus,
	  holdingStatus);
	  
	  String expectedClientName = clientCreationPage.getClientName();
	  
	  String actualClientName = homePage.searchForClient(expectedClientName);
	  
	  if (actualClientName.contains(expectedClientName)) {
	  
	  Assert.assertTrue(true, "Client name does not match"); }
	  
	  String actualClientStatus =
	  homePage.getClientSubmissionStatus(Constants.INDIVIDUAL_CLIENT_STATUS_TEXT);
	  
	  Assert.assertEquals(Constants.INDIVIDUAL_CLIENT_STATUS_TEXT,
	  actualClientStatus, "CLient status does not match");
	  
	  }
	 
		
	  @Test(priority = 5, dataProvider = "getNonIndividualData", enabled = true)
	  public void verifyNonIndividualClientCreation(String taxStatus, String
	  holdingStatus) throws AWTException, InterruptedException {
	  
	  ExtentReportListener.extentTest
	  .info(MarkupHelper.createLabel("Starting client creation for  " + taxStatus +
	  "", ExtentColor.GREEN));
	  
	  clientCreationPage = homePage.navigateToClientCreation(taxStatus,
	  holdingStatus);
	  
	  homePage = clientCreationPage.formFillUpForNonIndividualClient(taxStatus);
	  
	  String expectedClientName = clientCreationPage.getClientName();
	  
	  String actualClientName = homePage.searchForClient(expectedClientName);
	  
	  if (actualClientName.contains(expectedClientName)) {
	  
	  Assert.assertTrue(true, "Client name does not match"); }
	  
	  String actualClientStatus =
	  homePage.getClientSubmissionStatus(Constants.INDIVIDUAL_CLIENT_STATUS_TEXT);
	  
	  Assert.assertEquals(Constants.INDIVIDUAL_CLIENT_STATUS_TEXT,
	  actualClientStatus, "CLient status does not match");
	  
	  }
	 

	@DataProvider
	public Object[][] getIndividualData() {

		Object[][] data = new Object[2][2];

		data[0][0] = "Individual";
		data[0][1] = "Single";
		data[1][0] = "On Behalf Of Minor";
		data[1][1] = "Single";
		/* data[3][0] = "NRI - NRE"; data[3][1] = "Joint"; */

		return data;
	}

	@DataProvider
	public Object[][] getNonIndividualData() {

		Object[][] data = new Object[2][2];

		data[0][0] = "Company";
		data[0][1] = null;
		data[1][0] = "Trust";
		data[1][1] = null;
		/*
		 * data[1][0] = "On Behalf Of Minor"; data[1][1] = "Single";
		 */
		/* data[3][0] = "NRI - NRE"; data[3][1] = "Joint"; */

		return data;
	}

}
