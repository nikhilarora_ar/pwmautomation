package com.pwm.TestCases;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.pwm.BaseClass.TestBase;
import com.pwm.Pages.HomePage;
import Constants.Constants;
import com.pwm.Pages.LoginPage;

@Listeners(TestListeners.ExtentReportListener.class)
public class LoginPageTest extends TestBase
{	
	LoginPage loginPage;
	HomePage homePage;
	
	public LoginPageTest()
	{
		super();
	}

	@BeforeClass(alwaysRun=true)
	public void setUp()
	{
		initialization();
		Log.info("Application Launched Successfully");
		
		loginPage = new LoginPage();
	}
	
	@Test(priority=1, enabled=true)
	public void loginPageTitleTest()
	{
		String title = loginPage.validateLoginPageTitle();
		/*
		 * System.out.println("Test Case One with Thread Id:- " +
		 * Thread.currentThread().getId());
		 */
		Assert.assertEquals(title, Constants.LOGIN_PAGE_TITLE, "Login Page Title is not Matched");
		Log.info("Login Page Title Verified");
	}
	
	
	@Test(priority=2, enabled=true) 
	public void loginTest() throws InterruptedException
	{
		homePage = loginPage.login(property.getProperty("Username"),property.getProperty("Password"));
		/*
		 * System.out.println("Test Case two with Thread Id:- " +
		 * Thread.currentThread().getId());
		 */
		Log.info("Successfully Logged into PWM Workstation");
	}
}
